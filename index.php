<?php
require_once "vendor/autoload.php";

$interest = new \Nikita\InterestPack\Interest(5000, 10, 3);

echo $interest->compoundInterest(), '<br>';
echo $interest->simpleInterest(), '<br>';